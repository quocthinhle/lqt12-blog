---
title: Convex hull - Graham scan
publishDate: "2023-08-17T00:00:00"
author: Le Quoc Thinh
tag: algorithm
heroImage:
  src: https://res.cloudinary.com/sgroup/image/upload/v1692372161/convex-hull-graham-scan-1_dlwy4j.png
  author: idgeek
---

## Table of content

1.  [Introduce convex hull problem](/thoughts/configuracion-basica-ipv6#habilitar-ipv6-globalmente)
2.  [Basic vector skills reminders](/thoughts/configuracion-basica-ipv6#dirección-link-local)
3.  [Graham scan algorithm](/thoughts/configuracion-basica-ipv6#configuación-manual-de-una-dirección-link-local)
4.  [Implementation using Java](/thoughts/configuracion-basica-ipv6#mostrar-información)

## Convex hull problem, what is this?

Considering this problem on Leetcode: https://leetcode.com/problems/erect-the-fence

In computer geometry, a convex hull is the smallest set of points that contains all the input points within its border.

As the example you'll see below, those green nails are set of the points of a convex hull. Your task in a convex hull problem is to find those points.

<div style="text-align: center;">
  <img
    src="https://res.cloudinary.com/sgroup/image/upload/v1692373885/convex-hull-graham-scan-2_ypo8ol.png"
    alt="Convex Hull Example"
    style="display: inline-block;"
  />
</div>

## Prerequisites: basic vector skills

### Angle between two vectors
An angle is made between two vectors, it depends on the module of the two-vector, and the separation between them in space.

<div style="text-align: center;">
  <img
    src="https://res.cloudinary.com/sgroup/image/upload/v1692375008/convex-hull-graham-scan-3_bwe2iz.png"
    alt="Convex Hull Example"
    style="display: inline-block;"
  />
</div>

### Inner product
Inner product (a.k.a dot product, scalar product) is a math operator that returns a double/float. The result of inner product represent many things,
in scope of a convex hull problem, the most important application is:
    - Measure the similarity and directions of vectors. If the value of inner product is nearly to 1, you can assume that two vectors are nearly in the same direction.
    - Sometimes, I ask myself "why is that tho?", but I just leave and accept it. like Ariana Grande said: "math class... :/("

Inner product between two vectors:
<div style="text-align: center;">
  <img
    src="https://res.cloudinary.com/sgroup/image/upload/v1692416616/convex-hull-graham-scan-5_wejrcq.png"
    alt="Convex Hull Example"
    style="display: inline-block;"
  />
</div>

### Cross product
Two vectors can be multiplied using the "Cross product"
For understanding it better with visualizations, visit: https://www.mathsisfun.com/algebra/vectors-cross-product.html


### Examine if three points r making a counter-clockwise
To check if the point C in three points A, B, C making a left turn (counter-clockwise) from A, B. We use the cross product between the two vectors AB and AC.
If the cross product between two vectors AB and AC
    - Positive: then C is making a right turn from A to B.
    - Zero: then A, B and C are collinear.
    - Negative: C is making a left-turn (a.k.a counter-clockwise)

```java title="function that checks ccw"
    public static int ccw(Point2D a, Point2D b, Point2D c) {
        Vector2D AB = new Vector2D(a, b);
        Vector2D AC = new Vector2D(a, c);

        double crossProduct = Vector2D.cross(AC, AB);

        if (crossProduct == 0) {
            return 0;
        }

        if (crossProduct < 0) {
            return 1;
        }

        return -1;
    }
```


## Graham Scan algorithm

 - Input:
    ```java title="input.txt"
    N = 6
    0 0
    1 0
    2 0
    1 1
    2 1
    2 2
    ```
 - These points on the coordinate plane.

 <div style="text-align: center;">
  <img
    src="https://res.cloudinary.com/sgroup/image/upload/v1692417232/convex-hull-graham-scan-6_dt3xm6.png"
    alt="Convex Hull Example"
    style="display: inline-block;"
  />
</div>

 - Give a set a points, we sort and get the root a convex hull, which has lowest y-orientation, we'll pick the left most one in case of a tie. In the test case I mentioned above, the root point is now (0,0).

 - Sort the sub-array (from the second element to the end) based on its polar angle with the root point. In case of a tie, we'll consider the distance from the two points to the root.
   The point is now sorted:
    ```java title="sorted-array"
    A(0 0)
    B(1 0)
    C(2 0)
    F(2 1)
    D(1 1)
    E(2 2)
    ```

 - Push the first three elements into the convex-hull result set.

 - Iterate through the rest of the array, now we check if the next point in the list turns left or right from the two points on the top of the stack. If it turns left, we push this item on the stack. If it turns right, we remove the item on the top of the stack and repeat this process for remaining items. This step takes O(n)
 time.

 <div style="text-align: center;">
  <img
    src="https://res.cloudinary.com/sgroup/image/upload/v1692417574/convex-hull-graham-scan-7_yab499.png"
    alt="Convex Hull Example"
    style="display: inline-block;"
  />
</div>

 - Consider the example below
    - We first put the first three points 0, 1, 2 to the stack.
    - Then when iterate to the point 3, which is making a left turn from 1 and 2. Push the point 3 to the stack. The stack is now (0,1,2,3).
    - Go to the next iteration, the point we are examining is 4, which is making a right turn, so we pop the stack till the last three points make a left-turn, in this case, we pop the point 3 out of the stack, so the convex hull points set is now (0,1,2,4).
    - ... keep go on to the last point.


## Java Implementation
```java title="Point2D.java"
class Point2D implements Comparable<Point2D> {
    private final double y;
    private final double x;

    Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getY() {
        return y;
    }

    public double getX() {
        return x;
    }

    public static int ccw(Point2D a, Point2D b, Point2D c) {
        Vector2D AB = new Vector2D(a, b);
        Vector2D AC = new Vector2D(a, c);

        double crossProduct = Vector2D.cross(AC, AB);

        if (crossProduct == 0) {
            return 0;
        }

        if (crossProduct < 0) {
            return 1;
        }

        return -1;
    }

    public static double distance(Point2D a, Point2D b) {
        double d = Math.sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
        return d;
    }

    @Override
    public int compareTo(Point2D o) {
        if (this.y == o.y) {
            return Double.compare(this.x, o.x);
        }

        return Double.compare(this.y, o.y);
    }

    @Override
    public String toString() {
        return (int)(this.x) + " " + (int) this.y;
    }
}
```

```java title="Vector2D.java"
class Vector2D {
    private final double x;
    private final double y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D(Point2D B, Point2D A) {
        this.x = A.getX() - B.getX();
        this.y = A.getY() - B.getY();
    }

    public static double cosine(Vector2D a, Vector2D b) {
        return (a.x * b.x + a.y * b.y) / (Math.sqrt(a.x * a.x + a.y * a.y) * Math.sqrt(b.x * b.x + b.y * b.y));
    }

    public static double cross(Vector2D a, Vector2D b) {
        return a.x * b.y - a.y * b.x;
    }

    @Override
    public String toString() {
        return "Vector (" + this.x + ", " + this.y + ")";
    }
}
```


```java title="GramhamScan.java"
public class GrahamScan {
    public static void main(String[] args) {
        Point2D[] points = new Point2D[] {
            new Point2D(0, 0),
            new Point2D(1, 0),
            new Point2D(2, 0),
            new Point2D(2, 1),
            new Point2D(2, 2),
            new Point2D(1, 1)
        };

        int n = points.length;

        Arrays.sort(points);
        Point2D pivot = points[0];

        Arrays.sort(points, 1, points.length, (Point2D a, Point2D b) -> {
            int orientation = Point2D.ccw(pivot, a, b);

            if (orientation == 0) {
                return Double.compare(Point2D.distance(pivot, a), Point2D.distance(pivot, b));
            }

            return orientation > 0 ? -1 : 1;
        });

        for (Point2D i : points) {
            System.out.println(i);
        }

        Stack<Point2D> hull = new Stack<>();

        hull.push(points[0]);
        hull.push(points[1]);
        hull.push(points[2]);

        for (int i = 3; i < n; i += 1) {
            Point2D top = hull.pop();

            while (Point2D.ccw(hull.peek(), top, points[i]) < 0) {
                top = hull.pop();
            }

            hull.push(top);
            hull.push(points[i]);
        }

        System.out.println(hull.size());
    }
}

```
