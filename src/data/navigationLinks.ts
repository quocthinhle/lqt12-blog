import type { NavigationLink } from "~/lib/types";

const navigationLinks: NavigationLink[] = [
  { name: "About", href: "/" },
  { name: "Blog", href: "/thoughts" },
];

export default navigationLinks;
